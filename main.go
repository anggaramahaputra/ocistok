package main

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"math"
	"net/http"
	"time"
)

type RequestCalculationLoan struct {
	Principal     float64 `json:"principal" validate:"required"`
	MonthlyPeriod int     `json:"monthlyPeriod" validate:"required"`
	AnnualRate    float64 `json:"annualRate" validate:"required"`
	StartDate     string  `json:"startDate" validate:"required"`
}

type ResponseCalculationLoan struct {
	No                  int    `json:"no"`
	Date                string `json:"date"`
	MonthlyPayment      string `json:"monthlyPayment"`
	MonthlyPaymentBasic string `json:"monthlyPaymentBasic"`
	MonthlyRatePayment  string `json:"monthlyRatePayment"`
	RemainingPrincipal  string `json:"remainingPrincipal"`
}

var validate = validator.New()

func main() {

	//init echo framework
	e := echo.New()
	//init API
	e.POST("/calculate/loan", calculationLoan)
	//start API
	e.Logger.Fatal(e.Start(":8081"))
}

func calculationLoan(c echo.Context) error {
	var err error
	var resp []ResponseCalculationLoan
	var parseDate time.Time
	//read request
	req := new(RequestCalculationLoan)
	if err = c.Bind(req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	//validate struct
	if err = validate.Struct(req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	//parse date from string
	parseDate, err = time.Parse("2006-01-02", req.StartDate)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	//get list payment loan
	resp = getListPaymentLoan(parseDate, req.Principal, (req.AnnualRate / 100), req.MonthlyPeriod)

	return c.JSON(http.StatusOK, resp)
}

func getListPaymentLoan(date time.Time, principal, annualRate float64, monthlyPeriod int) (resp []ResponseCalculationLoan) {
	var monthlyPayment, remainingPrincipal, monthlyRatePayment, interestPayment, principalPayment float64
	//get monthly rate
	monthlyRate := annualRate / float64(monthlyPeriod)

	//get yearly rate
	yearlyRate := monthlyRate * 12

	//get monthly payment
	monthlyPayment = (monthlyRate * principal) / (1 - (math.Pow(1+monthlyRate, float64(-monthlyPeriod))))

	//init remaining principal
	remainingPrincipal = principal

	//start get list payment
	for i := 1; i <= monthlyPeriod; i++ {

		//get monthly payment rate
		monthlyRatePayment = (yearlyRate / 360) * 30 * remainingPrincipal

		//get interest payment
		interestPayment = remainingPrincipal * monthlyRate

		//get principal fix payment
		principalPayment = monthlyPayment - interestPayment

		//deduct remaining principal
		remainingPrincipal -= principalPayment

		if remainingPrincipal < 0 {
			remainingPrincipal = 0
		}
		//add data to list payment
		resp = append(resp, ResponseCalculationLoan{
			No:                  i,
			Date:                date.Add(time.Hour * 24 * time.Duration(i-1)).Format("2006-01-02"),
			MonthlyPayment:      fmt.Sprintf("%.2f", monthlyPayment),
			MonthlyPaymentBasic: fmt.Sprintf("%.0f", monthlyPayment-monthlyRatePayment),
			MonthlyRatePayment:  fmt.Sprintf("%.0f", monthlyRatePayment),
			RemainingPrincipal:  fmt.Sprintf("%.0f", remainingPrincipal),
		})

	}

	return
}
