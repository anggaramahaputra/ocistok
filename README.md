# Go calculation loans projects

[![Go Reference](https://pkg.go.dev/badge/golang.org/x/example.svg)](https://pkg.go.dev/golang.org/x/example)

This repository contains a collection of Go programs and libraries that
demonstrate the language, standard libraries, and tools.

## Clone the project

```
$ git clone https://gitlab.com/anggaramahaputra/ocistok.git
$ cd ocistok
```
## How to run

```
$ cd ocistok
$ go run main.go
```
## How to build

```
$ cd ocistok
$ go run build
```